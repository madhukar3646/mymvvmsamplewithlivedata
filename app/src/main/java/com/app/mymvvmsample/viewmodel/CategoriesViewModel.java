package com.app.mymvvmsample.viewmodel;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import com.app.mymvvmsample.model.CommonResponseModel;
import com.app.mymvvmsample.model.UserCategoryModel;
import com.app.mymvvmsample.utils.RetrofitApis;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesViewModel extends ViewModel {

    private MutableLiveData<List<UserCategoryModel>> usercategoryList;
    private MutableLiveData<String> messageToShow=new MutableLiveData<>();
    RetrofitApis retrofitApis=RetrofitApis.Factory.create();

    public void getUserCategories()
    {
        Call<CommonResponseModel> call= retrofitApis.categoryList("40");
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {
                CommonResponseModel body=response.body();
                if(body!=null) {
                    if(body.getStatus()==1)
                      usercategoryList.setValue(body.getUsercategories());
                    else
                        messageToShow.setValue(body.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {
              messageToShow.setValue(t.getMessage());
            }
        });
    }

    public void deleteCategory(String id)
    {
        Call<CommonResponseModel> call= retrofitApis.deleteUserCategory(id);
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {
                CommonResponseModel body=response.body();
                if(body!=null) {
                    if(body.getStatus()==0)
                        messageToShow.setValue("Category deleted successfully");
                    else
                        messageToShow.setValue(body.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {
               messageToShow.setValue(t.getMessage());
            }
        });
    }

    public MutableLiveData<List<UserCategoryModel>> getUsercategoryList()
    {
        if(usercategoryList==null)
            usercategoryList=new MutableLiveData<>();
        return usercategoryList;
    }

    public MutableLiveData<String> getMessageToShow()
    {
        if(messageToShow==null)
            messageToShow.setValue("");
        return messageToShow;
    }
}
