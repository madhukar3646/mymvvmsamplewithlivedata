package com.app.mymvvmsample.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.app.mymvvmsample.model.AddCategoryModel;
import com.app.mymvvmsample.model.CommonResponseModel;
import com.app.mymvvmsample.model.HashtagsDataModel;
import com.app.mymvvmsample.utils.RetrofitApis;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HashtagViewModel extends ViewModel {

    private MutableLiveData<List<HashtagsDataModel>> hashtaglist;
    private MutableLiveData<String> messageToShow=new MutableLiveData<>();
    private MutableLiveData<Boolean> isItemAdded=new MutableLiveData<>();
    RetrofitApis retrofitApis=RetrofitApis.Factory.create();

    public void getHashtagsData()
    {
        Call<CommonResponseModel> call= retrofitApis.hashTagList("0");
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {
                CommonResponseModel body=response.body();
                if(body!=null) {
                    if(body.getStatus()==1)
                        hashtaglist.setValue(body.getData());
                    else
                        messageToShow.setValue(body.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {
                messageToShow.setValue(t.getMessage());
            }
        });
    }

    public void addUserCategory(String userId,String latitude,String longitude,String place,String type)
    {
        Call<AddCategoryModel> call= retrofitApis.addUserCategory(userId,latitude,longitude,place,type);
        call.enqueue(new Callback<AddCategoryModel>() {
            @Override
            public void onResponse(Call<AddCategoryModel> call, Response<AddCategoryModel> response) {
                AddCategoryModel body=response.body();
                if(body!=null) {
                    if(body.getStatus()==0) {
                        messageToShow.setValue("Category added successfully");
                        isItemAdded.setValue(true);
                    }
                    else
                        messageToShow.setValue(body.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddCategoryModel> call, Throwable t) {
                messageToShow.setValue(t.getMessage());
            }
        });
    }

    public MutableLiveData<List<HashtagsDataModel>> getHashtagList()
    {
        if(hashtaglist==null)
            hashtaglist=new MutableLiveData<>();
        return hashtaglist;
    }

    public MutableLiveData<String> getMessageToShow()
    {
        return messageToShow;
    }

    public MutableLiveData<Boolean> getItemStatus()
    {
        return isItemAdded;
    }
}
