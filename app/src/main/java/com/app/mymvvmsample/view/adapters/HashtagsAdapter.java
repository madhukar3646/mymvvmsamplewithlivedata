package com.app.mymvvmsample.view.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.mymvvmsample.R;
import com.app.mymvvmsample.model.HashtagsDataModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Manish on 10/3/2017.
 * */

public class HashtagsAdapter extends RecyclerView.Adapter<HashtagsAdapter.ViewHolder>{

    private Activity context;
    private OnHashtagClickListener onHashtagClickListener;
    private ArrayList<HashtagsDataModel> hashtagsDataModelArrayList;

    public HashtagsAdapter(Activity context,ArrayList<HashtagsDataModel> hashtagsDataModelArrayList){
        this.context = context;
        this.hashtagsDataModelArrayList=hashtagsDataModelArrayList;
    }

    public void setOnHashtagClickListener(OnHashtagClickListener onHashtagClickListener)
    {
        this.onHashtagClickListener=onHashtagClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.hashtagmodel, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
      HashtagsDataModel model=hashtagsDataModelArrayList.get(position);
      holder.tv_categoryname.setText(model.getHashTag());
      holder.itemView.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(onHashtagClickListener!=null)
                  onHashtagClickListener.onHashtagClick(hashtagsDataModelArrayList.get(position));
          }
      });
    }

    @Override
    public int getItemCount() {
        return hashtagsDataModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_categoryname)
        TextView tv_categoryname;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public interface OnHashtagClickListener
    {
        void onHashtagClick(HashtagsDataModel model);
    }
}
