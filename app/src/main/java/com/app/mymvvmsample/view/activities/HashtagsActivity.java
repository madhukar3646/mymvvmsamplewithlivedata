package com.app.mymvvmsample.view.activities;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import com.app.mymvvmsample.R;
import com.app.mymvvmsample.model.HashtagsDataModel;
import com.app.mymvvmsample.view.adapters.HashtagsAdapter;
import com.app.mymvvmsample.viewmodel.HashtagViewModel;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HashtagsActivity extends BaseActivity implements View.OnClickListener,HashtagsAdapter.OnHashtagClickListener{

    @BindView(R.id.iv_backbtn)
    ImageView iv_backbtn;
    @BindView(R.id.rv_tagslist)
    RecyclerView rv_tagslist;
    private HashtagViewModel hashtagViewModel;
    private ArrayList<HashtagsDataModel> hashtagsDataModelArrayList=new ArrayList<>();
    private HashtagsAdapter hashtagsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hashtags);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        init();
    }

    private void init()
    {
      iv_backbtn.setOnClickListener(this);
      rv_tagslist.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
      hashtagsAdapter=new HashtagsAdapter(this,hashtagsDataModelArrayList);
      hashtagsAdapter.setOnHashtagClickListener(this);
      rv_tagslist.setAdapter(hashtagsAdapter);

      hashtagViewModel= ViewModelProviders.of(this).get(HashtagViewModel.class);
      hashtagViewModel.getHashtagList().observe(this, new Observer<List<HashtagsDataModel>>() {
          @Override
          public void onChanged(@Nullable List<HashtagsDataModel> hashtagsDataModels) {
              hashtagsDataModelArrayList.addAll(hashtagsDataModels);
              hashtagsAdapter.notifyDataSetChanged();
          }
      });
      hashtagViewModel.getMessageToShow().observe(this, new Observer<String>() {
          @Override
          public void onChanged(@Nullable String s) {
              showToast(s);
          }
      });
      hashtagViewModel.getItemStatus().observe(this, new Observer<Boolean>() {
          @Override
          public void onChanged(@Nullable Boolean aBoolean) {
              if(aBoolean)
                  finish();
          }
      });

      if(isInternetConnected())
          hashtagViewModel.getHashtagsData();
      else
          showInternetToast();
    }

    @Override
    public void onClick(View view){
        switch (view.getId())
        {
            case R.id.iv_backbtn:
                finish();
                break;
        }
    }

    @Override
    public void onHashtagClick(HashtagsDataModel model) {
       if(isInternetConnected())
           hashtagViewModel.addUserCategory("40","","",model.getHashTag(),"hashtag");
       else
           showInternetToast();
    }
}
