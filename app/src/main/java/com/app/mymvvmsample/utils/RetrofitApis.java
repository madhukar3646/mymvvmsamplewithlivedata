package com.app.mymvvmsample.utils;
import com.app.mymvvmsample.model.AddCategoryModel;
import com.app.mymvvmsample.model.CommonResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/**
 * Created by madhu on 8/2/2018.
 * */

public interface RetrofitApis {

        class Factory {
            public static RetrofitApis create() {

                // default time out is 15 seconds
                OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();

                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Utils.BASEURL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                return retrofit.create(RetrofitApis.class);
            }
        }

    @FormUrlEncoded
    @POST("categoryList")
    Call<CommonResponseModel> categoryList(@Field("userId") String userId);

    @FormUrlEncoded
    @POST("deleteUserCategory")
    Call<CommonResponseModel> deleteUserCategory(@Field("id") String id);

    @FormUrlEncoded
    @POST("hashTagList")
    Call<CommonResponseModel> hashTagList(@Field("offset") String offset);

    @FormUrlEncoded
    @POST("addUserCategory")
    Call<AddCategoryModel> addUserCategory(@Field("userId") String userId, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("place") String place, @Field("type") String type);
}

